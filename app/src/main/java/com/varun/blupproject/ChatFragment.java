package com.varun.blupproject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatFragment extends Fragment {
    ListView listView;
    TextView textView;
    CircleImageView circleImageView;

    String[]Heading={"Beautiful girl","Pretty girl","Beautiful girl","Pretty girl","Beautiful girl","Pretty girl","Beautiful girl","Pretty girl","Beautiful girl","Pretty girl"};
    int[]images={R.drawable.girl,R.drawable.girl2,R.drawable.girl4,R.drawable.girl6,R.drawable.girl4,R.drawable.girl,R.drawable.girl4,R.drawable.girl6,R.drawable.girl4,R.drawable.girl};
    int[]values={1,1,1,1,1,1,1,1,1,1};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_chat, container, false);
        listView=view.findViewById(R.id.list);
        ChatAdapter chatAdapter=new ChatAdapter(Heading,images,values,getContext());
        listView.setAdapter(chatAdapter);
        return view;
      /*  listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in=new Intent(getActivity(),ChatSubChat.class);
                getActivity().startActivity(in);

            }
        });*/
    }
}
