package com.varun.blupproject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ChatAdapter extends BaseAdapter {
    String[] head;
    int[] images;
    int[] valu;
    Context context;
    public ChatAdapter(String[] head, int[] img, int[] values, Context context) {
        this.head = head;
        images = img;
        this.valu = values;
        this.context=context;
    }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_chat_item, parent, false);
        TextView textView = view.findViewById(R.id.top);
        ImageView imageView = view.findViewById(R.id.girl);
        final TextView value = view.findViewById(R.id.text);
//        value.setText("" + valu[position]);
        textView.setText(head[position]);
        imageView.setImageResource(images[position]);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Welcome ", Toast.LENGTH_SHORT).show();
          /*      Intent in=new Intent(context,ChatSubChat.class);
                context.startActivity(in);
          */  }
        });
        return view;
    }
}
