package com.varun.blupproject;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;

public class App extends Application {
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        this.context=this;
        FirebaseApp.initializeApp(this);
    }
}
