package com.varun.blupproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.github.dhaval2404.imagepicker.ImagePicker;

public class ProfileFragment extends Fragment {
    Button button;
    Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_profile, container, false);
        button=view.findViewById(R.id.profilebtn);
         button.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
        Intent in=new Intent(context, ImagePicker.class);
        context.startActivity(in);
        }
        });
         return view;
    }
}

