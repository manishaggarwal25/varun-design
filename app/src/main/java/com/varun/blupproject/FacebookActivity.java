package com.varun.blupproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FacebookActivity extends AppCompatActivity {
    private CallbackManager mCallbackamanager;
    FirebaseAuth mAuth;
    LoginButton loginButton;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);
       /* preferences=getSharedPreferences("my shared prefrences",MODE_PRIVATE);
        editor=preferences.edit();*/
        mAuth= FirebaseAuth.getInstance();
        printHashKey();
        mCallbackamanager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.fblogin);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackamanager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(FacebookActivity.this, "cancel", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(FacebookActivity.this, error.toString(), Toast.LENGTH_SHORT).show();


            }
        });

}
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackamanager.onActivityResult(requestCode,resultCode,data);
    }
    public  void printHashKey() {
        try {
            PackageInfo info =getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("tag", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("tag", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("tag", "printHashKey()", e);
        }
    }
    private  void handleFacebookAccessToken(AccessToken accessToken){
        AuthCredential credential= FacebookAuthProvider.getCredential(accessToken.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            String name=task.getResult().getUser().getDisplayName();
                           /* editor.putString("id",task.getResult().getUser().getUid());
                            editor.putString("name",task.getResult().getUser().getDisplayName());
                            editor.commit();*/
                            Toast.makeText(FacebookActivity.this, "login SUCCESS", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(FacebookActivity.this,LoginActivity.class);
                            startActivity(intent);
                        }else
                        {
                            Toast.makeText(FacebookActivity.this,task.getException().toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Log.i("open","done",e);
            }
        });
    }
}

