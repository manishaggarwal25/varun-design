package com.varun.blupproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager1;
    TabLayout tabLayout;
    TextView next,skip;
    DotsIndicator dotsIndicator;
    Button msgbox, phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dotsIndicator=findViewById(R.id.dotIndicator);
        viewPager1=findViewById(R.id.viewpage);
        tabLayout=findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager1);
        next=findViewById(R.id.next);
        skip=findViewById(R.id.skip);
        BlubAdapter viewPager=new BlubAdapter(getSupportFragmentManager());
        viewPager.addFragment(new FirstFragment());
        viewPager.addFragment(new SecondFragment());
        viewPager1.setAdapter(viewPager);
        dotsIndicator.setViewPager(viewPager1);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager1.setCurrentItem(viewPager1.getCurrentItem()+1);
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
       /* msgbox =findViewById(R.id.msgbox);
        phone=findViewById(R.id.phone);
        msgbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_SEND);
                //intent.putExtra(Intent.EXTRA_EMAIL,new String[]{""});
                //intent.putExtra(Intent.EXTRA_SUBJECT,"HIIIIII");
                // intent.putExtra(Intent.EXTRA_TEXT,"NAVDEEP");
                intent.setType("text/plain");
                startActivity(intent);
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri u=Uri.parse("tel:9463121903");
                Intent intent=new Intent(Intent.ACTION_DIAL);
*//*                intent.setData(Uri.parse("mail to:"));
                intent.setType("text/plain");*//*
                startActivity(intent);
            }
        });


*/
    }
}


