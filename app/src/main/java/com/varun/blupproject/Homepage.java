package com.varun.blupproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class Homepage extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    BottomNavigationView navigation;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        drawerLayout = findViewById(R.id.draw);
        navigationView = findViewById(R.id.nav_view);
        navigation = findViewById(R.id.Bottom_Nav);
        toolbar=findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        SwipeFragment swipeFragment = new SwipeFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.Container, swipeFragment).commit();

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
               if (item.getItemId() == R.id.home_item) {
                    openfragment(new SwipeFragment());
                }
                else if (item.getItemId() == R.id.phone_item) {
                          openfragment(new FirstFragment());
                }
                else if (item.getItemId() == R.id.chats_item) {
                        openfragment(new ChatFragment());
                }
                else if (item.getItemId() == R.id.profile_item) {
                      openfragment(new ProfileFragment());
                }

              return true;
            }
        });



        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId()==R.id.setting){

                    Toast.makeText(Homepage.this, "Setting", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(Homepage.this,Setting.class);
                    startActivity(intent);
                }
                else  if(item.getItemId()==R.id.contact){

                    // Toast.makeText(HomePage.this, "Contact", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(Homepage.this,Contactus.class);
                    startActivity(intent);
                }
                else  if(item.getItemId()==R.id.abaout){

                    Toast.makeText(Homepage.this, "AboutUs", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(Homepage.this,Aboutus.class);
                    startActivity(intent);
                }
                else  if(item.getItemId()==R.id.logout){

                    Toast.makeText(Homepage.this, "LogOut", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(Homepage.this,SignIn.class);
                    startActivity(intent);
                }
                return true;
            }
        });
      /* navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId()==R.id.contact){

                }
                return true;
            }
        });*/

    }



    public void openfragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.Container, fragment).commit();
    }
    }


