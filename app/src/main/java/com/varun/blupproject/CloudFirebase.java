package com.varun.blupproject;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class CloudFirebase extends AppCompatActivity {
FirebaseFirestore firestore;
EditText text1,text2, text3;
Button button;

@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_cloud_firebase);
text1=findViewById(R.id.e1);
text2=findViewById(R.id.e2);
text3=findViewById(R.id.e3);
button=findViewById(R.id.b1);
firestore = FirebaseFirestore.getInstance();
button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Map<String,Object> map=new HashMap<>();
        map.put("firstname",text1.getText().toString());
        map.put("lastname", text2.getText().toString());
        map.put("email",text3.getText().toString());

        firestore.collection("user").
                add(map).
                addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        Toast.makeText(CloudFirebase.this, task.toString(), Toast.LENGTH_SHORT).show();

                    }
                }).
                addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference aVoid) {
                        Toast.makeText(CloudFirebase.this, "successfully added", Toast.LENGTH_SHORT).show();

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CloudFirebase.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
});

}
}

