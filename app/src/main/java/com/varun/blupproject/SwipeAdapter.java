package com.varun.blupproject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.yuyakaido.android.cardstackview.CardStackView;

public class SwipeAdapter extends CardStackView.Adapter<SwipeAdapter.SwipeViewHolder> {
    int[]images;
    public SwipeAdapter(int[]images){
        this.images=images;
    }
    @NonNull
    @Override
    public SwipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from((parent.getContext())).inflate(R.layout.fragment_swipe,parent,false);
        return new SwipeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SwipeViewHolder holder, int position) {
        holder.imageView.setImageResource(images[position]);
        return;
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

        class SwipeViewHolder extends CardStackView.ViewHolder{
            ImageView imageView;

            public SwipeViewHolder(View viewHolder)
            {
                super(viewHolder);
                imageView=viewHolder.findViewById(R.id.imgg);

            }
    }
}
