package com.varun.blupproject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;

public class SwipeFragment extends Fragment implements CardStackListener {
    CardStackView cardStackView;
    SwipeAdapter adapter;
    int[]images={R.drawable.colorwallppr,R.drawable.chatgirl,R.drawable.boogie_handyman1,R.drawable.lifeisgood,R.drawable.smile};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_swipe, container, false);
        cardStackView=view.findViewById(R.id.cardstack);
        cardStackView.setLayoutManager(new CardStackLayoutManager(getContext(),this));
        adapter=new SwipeAdapter(images);
        cardStackView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {

    }

    @Override
    public void onCardSwiped(Direction direction) {

    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {

    }

    @Override
    public void onCardDisappeared(View view, int position) {

    }
}
