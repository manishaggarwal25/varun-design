package com.varun.blupproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class IntroPage extends AppCompatActivity {
    TextView next,skip;
    ViewPager viewPager;
    DotsIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_page);
        viewPager=findViewById(R.id.viewPager);
       indicator=findViewById(R.id.dot1);
        next=findViewById(R.id.txt1);
        skip=findViewById(R.id.txt21);
        BlubAdapter adapter=new BlubAdapter(getSupportFragmentManager());
        adapter.addFragment(new FirstFragment());
        adapter.addFragment(new SecondFragment());
        adapter.addFragment(new ThirdFragment());
        adapter.addFragment(new FourthFragment());
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1);

            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(IntroPage.this,LoginActivity.class);
                startActivity(intent);

            }
        });
    }
}


