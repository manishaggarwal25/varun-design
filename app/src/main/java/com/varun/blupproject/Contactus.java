package com.varun.blupproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Contactus extends AppCompatActivity {
    TextView contact;
    Button msgbox,phone,send;
    EditText name,mail1,message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        contact=findViewById(R.id.contact);
        msgbox=findViewById(R.id.msgbox);
        send=findViewById(R.id.send);
        mail1=findViewById(R.id.emailedit);
        name=findViewById(R.id.name);
        message=findViewById(R.id.msg);
        phone=findViewById(R.id.phone);
        msgbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{""});
                intent.putExtra(Intent.EXTRA_SUBJECT,"HIIIIII");
                 intent.putExtra(Intent.EXTRA_TEXT,"NAVDEEP");
                intent.setType("text/plain");
                startActivity(intent);
            }
        });
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri u=Uri.parse("tel:9463121903");
                Intent intent=new Intent(Intent.ACTION_DIAL);
               // intent.setData(Uri.parse("mail to:"));
                //intent.setType("text/plain");
                startActivity(intent);
            }
        });

    }
}
