package com.varun.blupproject;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.github.dhaval2404.imagepicker.ImagePicker;

public class ProfileAdapter extends BaseAdapter {
    Button button;
    Context context;

    public ProfileAdapter(Context context) {
        this.context =context;
    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context, ImagePicker.class);
                context.startActivity(in);
            }
        });
        return convertView;
    }
}
