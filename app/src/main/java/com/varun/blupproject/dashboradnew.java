package com.varun.blupproject;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class dashboradnew extends AppCompatActivity {

    RecyclerView recyclerView;
    DashboardAdapter adapter;
    int[]images={R.drawable.edit_profile1,R.drawable.shooping_list1,R.drawable.reward_pont1,R.drawable.boogie_wallet1,R.drawable.available_delivery1,R.drawable.boogie_vip1,
            R.drawable.boogie_handyman1,R.drawable.loan,R.drawable.message_box1};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboradnew);
        recyclerView=findViewById(R.id.Recycle);
        adapter=new DashboardAdapter(images);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);

    }
}
